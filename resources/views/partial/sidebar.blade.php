<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
      <li class="nav-item">
        <a class="nav-link" href="/">
          <i class="icon-grid menu-icon"></i>
          <span class="menu-title">Dashboard</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#cast" aria-expanded="false" aria-controls="cast">
          <i class="ti-video-clapper menu-icon"></i>
          <span class="menu-title">Caster</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="cast">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="/cast">List Caster</a></li>
            {{-- <li class="nav-item"> <a class="nav-link" href="pages/ui-features/dropdowns.html">Tambah Caster</a></li> --}}
          </ul>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#genre" aria-expanded="false" aria-controls="genre">
          <i class="ti-layout-menu-v menu-icon"></i>
          <span class="menu-title">Genre</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="genre">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"><a class="nav-link" href="/genre">List Genre</a></li>
          </ul>
        </div>
      </li>

          
      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#film" aria-expanded="false" aria-controls="film">
          <i class="ti-video-camera menu-icon"></i>
          <span class="menu-title">Film</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="film">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="/film">List Film</a></li>
          </ul>
        </div>
      </li>

@auth
      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#peran" aria-expanded="false" aria-controls="peran">
          <i class="ti-notepad menu-icon"></i>
          <span class="menu-title">Peran</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="peran">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="/peran">List Peran</a></li>
          </ul>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#kritik" aria-expanded="false" aria-controls="kritik">
          <i class="ti-comments menu-icon"></i>
          <span class="menu-title">Kritik</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="kritik">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="/kritik">List Kritik</a></li>
          </ul>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#profil" aria-expanded="false" aria-controls="profil">
          <i class="ti-user menu-icon"></i>
          <span class="menu-title">Profil User</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="profil">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="/profila">List Profil User</a></li>
          </ul>
        </div>
      </li>
@endauth

    </ul>
  </nav>