<nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
    <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
      <a class="navbar-brand brand-logo mr-5" href="index.html"><img src="https://member.pksdigitalschool.id/images/logos/logotype.png" class="mr-2" alt="logo"/></a>
      <a class="navbar-brand brand-logo-mini" href="index.html"><img src="https://member.pksdigitalschool.id/images/logos/logogram.png" alt="logo"/></a>
    </div>
    <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
      <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
        <span class="icon-menu"></span>
      </button>

      <ul class="navbar-nav navbar-nav-right">

        <li class="nav-item nav-profile dropdown">
          <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
            <img src="{{asset('theme/images/faces/face28.jpg')}}" alt="profile"/>
          </a>
@guest
          <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
            <a class="dropdown-item" href="{{ route('login') }}" 
                onclick="event.preventDefault();
                document.getElementById('login-form').submit();">
              <form id="login-form" action="{{ route('login') }}" method="get" style="display: none;">
                @csrf
              </form>
              <i class="ti-power-off text-primary"></i>
              Login
            </a>
          </div>
@endguest
@auth    
          <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
            <a class="dropdown-item" href="{{ route('logout') }}" 
                onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
              </form>
              <i class="ti-power-off text-primary"></i>
              Logout
            </a>
          </div>
@endauth
        </li>
      </ul>

    </div>
  </nav>