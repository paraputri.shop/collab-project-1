@extends('layout.master')
@section('title')
SISTEM INFORMASI BIOSKOP
@endsection
@section('content')
    <h3>Selamat Datang di Sistem Informasi Bioskop</h3>
    <br>
    <h4>Catatan Rilis v.1:</h4>
    <ul>
        <li>CRUD data Caster, Genre, dan Film ( data Peran, Kritik dan Profile on progress)</li>
        <li>Autentikasi dan Middleware: </li>
            <ul>- Register</ul>
            <ul>- Login</ul>        
            <ul>- Autentikasi Dashboard bisa login dan tidak login</ul>        
            <ul>- Autentikasi tombol Tambah, Edit dan Delete setiap menu</ul>      
        <li>Disiapkan Oleh: </li>
            <ul>- Nasir Mahmud Ahmad</ul>
            <ul>- Hendrik Zulmitra</ul>        
</ul>
@endsection