<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Register Page</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="{{asset('theme/vendors/feather/feather.css')}}">
  <link rel="stylesheet" href="{{asset('theme/vendors/ti-icons/css/themify-icons.css')}}">
  <link rel="stylesheet" href="{{asset('theme/vendors/css/vendor.bundle.base.css')}}">
  <!-- endinject -->
  <!-- Plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="{{asset('theme/css/vertical-layout-light/style.css')}}">
  <!-- endinject -->
  <link rel="shortcut icon" href="{{asset('theme/images/favicon.png')}}" />
</head>

<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth px-0">
        <div class="row w-100 mx-0">
          <div class="col-lg-4 mx-auto">
            <div class="auth-form-light text-left py-4 px-2 px-sm-5">
              <div class="brand-logo">
                <img src="https://member.pksdigitalschool.id/images/logos/logogram.png" style="width:100px;height:100px;" alt="logo">
                <img src="https://member.pksdigitalschool.id/images/logos/logotype.png" alt="logo">
              </div>
              <h4>Mau Daftar?</h4>
              <h6 class="font-weight-light">Silakan isi form Registrasi di bawah ini.</h6>

              {{-- <form action="{{ route('register') }}" method="post"> --}}

              <form class="pt-3" method="POST" action="{{ route('register') }}">
                @csrf
                <div class="form-group">
                  <input type="text" name="name" class="form-control form-control-lg" placeholder="Nama Lengkap">
                </div>
                @error('name')
                <div class="alert alert-warning">{{ $message }}</div>
                @enderror
                <div class="form-group">
                  <input type="email" name="email" class="form-control form-control-lg" placeholder="Email">
                </div>
                @error('email')
                  <div class="alert alert-warning">{{ $message }}</div>
                @enderror
                
                <div class="form-group">
                  <input type="password" name="password" class="form-control form-control-lg" placeholder="Password">
                </div>
                @error('password')
                  <div class="alert alert-warning">{{ $message }}</div>
                @enderror
                <div class="form-group">
                  <input type="password" name="password_confirmation" class="form-control form-control-lg" placeholder="Retype password">
                </div>

                <div class="form-group">
                  <input type="number" class="form-control form-control-lg" name="umur" placeholder="Input umur"> 
                </div>
                @error("umur")
                  <div class="alert alert-warning">{{ $message }}</div>
                @enderror
        
                <div class="form-group">
                  <textarea name="bio" class="form-control form-control-lg" placeholder="Isi biodata"></textarea>  
                </div>
                @error('bio')
                  <div class="alert alert-warning">{{ $message }}</div>
                @enderror
        
                <div class="form-group">
                  <textarea name="alamat" class="form-control form-control-lg" placeholder="Isi alamat"></textarea>  
                </div>
                @error('alamat')
                  <div class="alert alert-warning">{{ $message }}</div>
                @enderror
                
                <!-- /.col -->
                <div class="mt-3">
                  <button type="submit" class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn">REGISTER</button>
                </div>
                <!-- /.col -->

                <div class="text-center mt-4 font-weight-light">
                  Sudah punya akun? <a href="/login" class="text-primary">Login</a>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="{{asset('theme/vendors/js/vendor.bundle.base.js')}}"></script>
  <!-- endinject -->
  <!-- Plugin js for this page -->
  <!-- End plugin js for this page -->
  <!-- inject:js -->
  <script src="{{asset('theme/js/off-canvas.js')}}"></script>
  <script src="{{asset('theme/js/hoverable-collapse.js')}}"></script>
  <script src="{{asset('theme/js/template.js')}}"></script>
  <script src="{{asset('theme/js/settings.js')}}"></script>
  <script src="{{asset('theme/js/todolist.js')}}"></script>
  <!-- endinject -->
</body>

</html>
