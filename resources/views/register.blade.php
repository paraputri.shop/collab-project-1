@extends('layout.master')

@section('judul')
Halaman From 
@endsection

@section('content')
    <h1>Buat Account Baru</h1>
    <h3>Sign up form</h3>
    
    <form action="welcome" method="post">
        @csrf 
        <label>First name:</label><br><br>
        <input type="text" name="fname"><br><br>
        <label>Lase name:</label><br><br>
        <input type="text" name="lname"><br><br>
        <label>Gender</label><br><br>
        <input type="radio" name="name"> Male <br>
        <input type="radio" name="name"> Famale <br><br>
        <label>Nationality</label><br><br>
        <select name="Nationality">
            <option value="Indonesia">Indonesia</option>
            <option value="Amerika">Amerika</option>
            <option value="English">English</option>
        </select> <br><br>

        <label>Language Spoken</label><br><br>
        <input type="checkbox" name="Language Spoken"> Bahasa Indonesia<br>
        <input type="checkbox" name="Language Spoken"> English<br>
        <input type="checkbox" name="Language Spoken"> Other<br><br>
        <label>Bio</label><br>
        <textarea name="Bio" rows="10" cols="30"></textarea><br>

        <input type="submit" value="Sign Up">

    </form>
@endsection