@extends('layout.master')

@section('title')
Update Profile
@endsection

@section('content')

<form method="POST" action="/profila/{{ $profila->id }}">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama User</label>
      <input type="text" value="{{ $profila->user->name }}" class="form-control" disabled> 
    </div>
    @error('nama')
        <div class="alert alert-warning">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Email User</label>
      <input type="text" value="{{ $profila->user->email }}" class="form-control" disabled>
    </div>
    @error('email')
        <div class="alert alert-warning">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Umur</label>
      <input type="number" name="umur" value="{{ $profila->umur }}" class="form-control">
    </div>
    @error('umur')
        <div class="alert alert-warning">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Biodata</label>
      <textarea name="bio" cols="30" rows="10" class="form-control">{{ $profila->bio }}</textarea>
    </div>
    @error('bio')
        <div class="alert alert-warning">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Alamat</label>
        <textarea name="alamat" cols="30" rows="10" class="form-control">{{ $profila->alamat }}</textarea>
      </div>
      @error('alamat')
          <div class="alert alert-warning">{{ $message }}</div>
      @enderror
    <button type="submit" class="btn-primay">Submit</button>
</form>

@endsection