@extends('layout.master')

@section('title')
List Genre 
@endsection

@section('content')
@auth
  <a href="/genre/create" class="btn btn-primary mb-3 btn-sm">Tambah Genre</a>    
@endauth

<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">List Film</th>
        <th scope="col">Action</th>
      </tr>
    </thead>  
    <tbody>
        @forelse ($genre as $key =>$item)
        <tr>
            <td>{{ $key + 1 }}</td>
            <td>{{ $item->nama }}</td>
            <td>
                <ul>
                    @foreach ($item->film as $value)
                      <li>{{ $value->judul }}</li>
                    @endforeach
                </ul>
            </td>
            
              
              <td>
                <form class="mt-2" action="/genre/{{ $item->id }}" method="POST">
                    <a href="/genre/{{ $item->id }}" class="btn btn-info btn-sm">Detail</a>
                    @auth
                      <a href="/genre/{{ $item->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                      @csrf
                      @method("delete")
                      <input type="submit" value="Delete" class="btn btn-danger btn-sm">                        
                    @endauth
                </form>
            </td>
        </tr>
        @empty
            <h5 style="color:red; width:100%; text-align:center;">Data Genre Tidak Ada</h5>
        @endforelse
    </tbody>
  </table> 

@endsection