@extends('layout.master')

@section('title')
Detail Genre
@endsection

@section('content')
<form action="/genre/{{$genre->id}}" method="POST">
    @csrf
    @method('delete')
    <a href="/genre" class="btn btn-light btn-sm">Kembali</a>
    @auth
        <a href="/genre/{{$genre->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
        <input type="submit" value="delete" class="btn btn-danger btn-sm">        
    @endauth
</form>
<br>

<h4>Nama Genre : <br>{{ $genre->nama }}</h4>

<div class="row">
    @foreach ($genre->film as $item)
        <div class="col-4">
            <div class="card">
                <img src="{{ asset('gambar/'. $item->poster) }}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h3>{{ $item->judul }}</h3>
                    <p class="card-text"> {{ $item->ringkasan }} </p>
                </div>
            </div>
        </div>
    @endforeach
</div>



@endsection