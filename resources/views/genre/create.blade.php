@extends('layout.master')

@section('title')
Tambah Data Genre 
@endsection

@section('content')

<form method="POST" action="/genre">
    @csrf
    <div class="form-group">
      <label>Nama Genre</label>
      <input type="text" name="nama"  class="form-control">
    </div>
    @error('nama')
        <div class="alert alert-warning">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-success btn-sm">Simpan</button>
    <a href="/genre" class="btn btn-light btn-sm">Kembali</a>
</form>

@endsection