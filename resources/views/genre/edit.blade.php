@extends('layout.master')

@section('title')
Edit Genre 
@endsection

@section('content')

<form method="POST" action="/genre/{{ $genre->id }}">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama Genre</label>
      <input type="text" name="nama" value="{{ $genre->nama }}" class="form-control">
    </div>
    @error('nama')
        <div class="alert alert-warning">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-success btn-sm">Simpan</button>
    <a href="/genre" class="btn btn-light btn-sm">Kembali</a>
</form>

@endsection