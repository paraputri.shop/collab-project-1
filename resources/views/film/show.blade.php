@extends('layout.master')

@section('title')
Detail Film "{{ $film->judul }}"
@endsection

@section('content')
<form action="/film/{{$film->id}}" method="POST">
    @csrf
    @method('delete')
    <a href="/film" class="btn btn-light btn-sm">Kembali</a>
    @auth
        <a href="/film/{{$film->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
        <input type="submit" value="delete" class="btn btn-danger btn-sm">        
    @endauth

</form>
<br>
<img class="float-left" src="{{ asset('gambar/'. $film->poster)}}" alt="">
<div class="card" style="">
    <div class="card-body" style="align-content: left">
        <h3>Judul:</h3><h4>{{ $film->judul }}</h4><br>
        <h3>Ringkasan:</h3><h4>{{ $film->ringkasan }}</h4><br>
        <h3>Tahun:</h3><h4>{{ $film->tahun }}</h4><br>
        <h3>Genre:</h3><h4>{{ $film->genre->nama }}</h4>
    </div>
</div>

@endsection