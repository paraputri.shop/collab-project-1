@extends('layout.master')

@section('title')
List Data Film 
@endsection

@section('content')
@auth
    <a href="/film/create" class="btn btn-primary mb-2 btn-sm">Tambah Film</a>    
@endauth

<div class="row">
    @forelse ($film as $item)
        <div class="col-4 mt-3">
            <div class="card">
                <img src="{{ asset('gambar/'. $item->poster)}}" class="card-img-top" alt="...">
                <div class="card-body">
                <span class="badge badge-info">{{ $item->genre->nama}}</span>
                <h5 class="mt-3">{{ $item->judul }}</h5>
                <p class="card-text">{{Str::limit($item->ringkasan, 120 )}}</p>
                {{-- @auth     --}}
                <form action="/film/{{ $item->id }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <a href="/film/{{ $item->id }}" class="btn btn-info btn-sm">Detail</a>
                    @auth
                        <a href="/film/{{ $item->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                        <input type="submit" value="Delete" class="btn btn-danger btn-sm">    
                    @endauth
                </form>
                {{-- @endauth --}}
                </div>
            </div>
        </div> 
    @empty
        <h5 style="color:red; width:100%; text-align:center;">Data Film Belum Ada</h5>
    @endforelse    
</div>

@endsection