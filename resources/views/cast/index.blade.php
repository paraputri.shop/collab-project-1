@extends('layout.master')

@section('title')
List Nama Caster 
@endsection

@section('content')
@auth
  <a href="/cast/create" class="btn btn-primary mb-3 btn-sm">Tambah Caster</a> 
@endauth

<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Action</th>
      </tr>
    </thead>  
    <tbody>
        @forelse ($cast as $key =>$item)
        <tr>
            <td>{{ $key + 1 }}</td>
            <td>{{ $item->nama }}</td>
            <td>{{ $item->umur }}</td>
            <td>{{ $item->bio }}</td>
            <td>
              
            
                <form class="mt-2" action="/cast/{{ $item->id }}" method="POST">
                    <a href="/cast/{{ $item->id }}" class="btn btn-info btn-sm">Detail</a>
                    @auth                        
                      <a href="/cast/{{ $item->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                      @csrf
                      @method("delete")
                      <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                    @endauth
                </form>
            </td>
        </tr>
        @empty
            <h5 style="color:red; width:100%; text-align:center;">Data Caster Tidak Ada</h5>
        @endforelse
    </tbody>
  </table> 

@endsection