@extends('layout.master')

@section('title')
Detail Info Caster an. {{ $cast->nama }}
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('delete')
    <a href="/cast" class="btn btn-light btn-sm">Kembali</a>
    @auth
        <a href="/cast/{{$cast->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
        <input type="submit" value="delete" class="btn btn-danger btn-sm">    
    @endauth
    </form>
<br>
<h4>Nama Pemain : <br>{{ $cast->nama }}</h4>
<h4>Umur Pemain : <br>{{ $cast->umur }}</h4>
<h4> Bio Pemain : <br>{{ $cast->bio }}</h4>

@endsection