@extends('layout.master')

@section('title')
Edit Data Caster 
@endsection

@section('content')

<form method="POST" action="/cast/{{ $cast->id }}">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama Caster</label>
      <input type="text" name="nama" value="{{ $cast->nama }}" class="form-control">
    </div>
    @error('nama')
        <div class="alert alert-warning">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Umur</label>
      <input type="number" name="umur" value="{{ $cast->umur }}" class="form-control">
    </div>
    @error('umur')
        <div class="alert alert-warning">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Biodata</label>
      <textarea name="bio" cols="30" rows="10" class="form-control">{{ $cast->bio }}</textarea>
    </div>
    @error('bio')
        <div class="alert alert-warning">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-success btn-sm">Simpan</button>
    <a href="/cast" class="btn btn-light btn-sm">Kembali</a>

</form>

@endsection