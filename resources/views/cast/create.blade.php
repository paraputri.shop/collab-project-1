@extends('layout.master')

@section('title')
Tambah Caster Baru 
@endsection

@section('content')

<form method="POST" action="/cast">
    @csrf
    <div class="form-group">
      <label>Nama Caster</label>
      <input type="text" name="nama"  class="form-control">
    </div>
    @error('nama')
        <div class="alert alert-warning">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Umur</label>
      <input type="number" name="umur" class="form-control">
    </div>
    @error('umur')
        <div class="alert alert-warning">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Biodata</label>
      <textarea name="bio" cols="30" rows="10" class="form-control"></textarea>
    </div>
    @error('bio')
        <div class="alert alert-warning">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-success btn-sm">Simpan</button>
    <a href="/cast" class="btn btn-light btn-sm">Kembali</a>
</form>

@endsection