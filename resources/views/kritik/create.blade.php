@extends('layout.master')

@section('title')
Beri Kritik
@endsection

@section('content')

<h1>Kritik</h1>

@foreach ($film->kritik as $item)
    <div class="card">
        <div class="=card-body">
            <small> <b>{{ $item->user->name }}</b></small>
            <p class="card-text"> {{ $item->isi }}</p>
            <p class="card-text"> Point : {{ $item->point }}</p>
        </div>
    </div>
    
@endforeach

<form method="POST" action="/kritik" enctype="multipart/form-data" class="my-3">
    @csrf
    <div class="form-group">
      <label>Content</label>
      <input type="hidden" name="film_id" value="{{ $film->id }}" id="">
      <textarea name="isi" cols="30" rows="10" class="form-control"></textarea>
    </div>
    @error('isi')
        <div class="alert alert-warning">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Nilai</label>
      <input type="hidden" name="film_id" value="{{ $film->id }}" id="">
      <input type="number" name="point" class="form-control"> 
    </div>
    @error('point')
        <div class="alert alert-warning">{{ $message }}</div>
    @enderror
     
    <button type="submit" class="btn-primay">Submit</button>
</form>

<a href="/film" class="btn btn-secondary">Kembali</a>

@endsection