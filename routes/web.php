<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DashboardController@index');

Route::get('/register', 'FormController@pendaftaran');
  
Route::post('/welcome', 'FormController@kirim');

Route::get('/data-table', function(){
    return view('table.data-table');
});

    // READ
    Route::get('/cast', 'CastController@index'); //ambil data dari database ditampilkan di blade

    Route::get('/film', 'FilmController@index'); //ambil data dari database ditampilkan di blade

    Route::get('/genre', 'GenreController@index'); //ambil data dari database ditampilkan di blade

Route::group(['middleware' => ['auth']], function () {

    // CRUD cast
    // CREATE
    Route::get('/cast/create', 'CastController@create'); // mengarah ke from data
    Route::post('/cast', 'CastController@Store'); //menyimpan data from ke database

    //UPDATE
    Route::get('/cast/{cast_id}/edit', 'CastController@edit');// ROUTe untuk mengarah menu edit
    Route::put('/cast/{cast_id}/', 'CastController@update');// ROUTe untuk mengarah menu edit
    //DELETE
    Route::delete('/cast/{cast_id}', 'CastController@destroy');//ROUTE delete

    // CRUD GENRE
    Route::resource('genre', 'GenreController')->except([
        'index', 'show'
        ]);

    //CRUD FILM
    Route::resource('film', 'FilmController')->except([
        'index', 'show'
        ]);

    //Update PROFILA
    Route::resource('profila', 'ProfilaController')->only([
        'index', 'update'
        ]);

    //Update KRITIK
    Route::resource('kritik', 'KritikController')->only([
        'index', 'store'
        ]);
});

Route::get('/cast/{cast_id}', 'CastController@show'); //route detail cast
Route::get('/genre/{genre_id}', 'GenreController@show'); //route detail film
Route::get('/film/{film_id}', 'FilmController@show'); //route detail film

//login register
Auth::routes();


