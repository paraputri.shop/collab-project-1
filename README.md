## FINAL PROJECT 1

## Grup 1 - Kelompok 8

- **Nasir Mahmud Ahmad**
- **Hendrik Zulmitra**

## Tema Project

Dashboard Sistem Informasi Bioskop sederhana.

## ERD

<img src="https://gitlab.com/paraputri.shop/collab-project-1/-/raw/master/ERD.jpg" alt="">

## Link

Link Demo: http://pds.itstime.co.id/
<br>Link Video: https://youtu.be/X2HMrbAQZhI

