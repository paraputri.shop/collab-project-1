<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class profila extends Model
{
    protected $table = 'profila';

    protected $fillable = ['umur', 'bio', 'alamat', 'user_id'];
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
